package com.wikitude.samples;

import android.os.AsyncTask;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

public class DownloadWtc extends AsyncTask<String,Void,Boolean> {

    private MainActivity mainActivity;

    public DownloadWtc(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        System.out.println(">>>>STARTING DOWNLOADING FILE");
    }

    @Override
    protected Boolean doInBackground(String... strings) {

        try{
            URL u = new URL("http://ame97software.altervista.org/wikiArt/"+strings[0]);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();

            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            FileOutputStream f = new FileOutputStream(new File(mainActivity.getExternalFilesDir(null),strings[0]));
            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ( (len1 = in.read(buffer)) > 0 ) {
                f.write(buffer,0, len1);
            }
            f.close();
        } catch (ProtocolException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        System.out.println(">>>>FINISHED"+aBoolean);
        String s = "Aggiornamento completato";
        if(!aBoolean)
            s = "Aggiornamento fallito";
        Toast.makeText(this.mainActivity,s, Toast.LENGTH_SHORT).show();
    }
}
