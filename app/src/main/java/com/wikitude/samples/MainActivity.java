package com.wikitude.samples;


//Author: Alessandro Amedei, Gian Maria Pandolfi, Corso Vignoli
//Date: 25/06/2019

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wikitude.WikitudeSDK;
import com.wikitude.common.CallStatus;
import com.wikitude.common.devicesupport.Feature;
import com.wikitude.common.permission.PermissionManager;
import com.wikitude.common.util.SDKBuildInformation;
import com.wikitude.nativesdksampleapp.R;
import com.wikitude.samples.camera.CameraSettingsActivity;
import com.wikitude.samples.plugins.BarcodePluginActivity;
import com.wikitude.samples.plugins.FaceDetectionPluginActivity;
import com.wikitude.samples.plugins.SimpleInputPluginActivity;
import com.wikitude.samples.plugins.CustomCameraPluginActivity;
import com.wikitude.samples.rendering.external.ExternalRenderingActivity;
import com.wikitude.samples.rendering.internal.InternalRenderingActivity;
import com.wikitude.samples.tracking.cloud.ContinuousCloudRecognitionActivity;
import com.wikitude.samples.tracking.cloud.SingleCloudRecognitionActivity;
import com.wikitude.samples.tracking.image.ExtendedImageTrackingActivity;
import com.wikitude.samples.tracking.image.MultipleTargetsImageTrackingActivity;
import com.wikitude.samples.tracking.image.SimpleImageTrackingActivity;
import com.wikitude.samples.tracking.instant.InstantScenePickingActivity;
import com.wikitude.samples.tracking.instant.InstantTrackingActivity;
import com.wikitude.samples.tracking.instant.LoadInstantTargetActivity;
import com.wikitude.samples.tracking.instant.PlaneDetectionActivity;
import com.wikitude.samples.tracking.instant.SaveInstantTargetActivity;
import com.wikitude.samples.tracking.object.ExtendedObjectTrackingActivity;
import com.wikitude.samples.tracking.object.ObjectTrackingActivity;
import com.wikitude.samples.util.SampleCategory;
import com.wikitude.samples.util.SampleData;
import com.wikitude.samples.util.adapter.SamplesExpendableListAdapter;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;


public class MainActivity extends AppCompatActivity{

    private static final int EXPANDABLE_INDICATOR_START_OFFSET = 60;
    private static final int EXPANDABLE_INDICATOR_END_OFFSET = 30;

    private ExpandableListView listView;

    private final List<SampleCategory> sampleCategories = new ArrayList<>();
    private MainActivity mainActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mainActivity=this;

     //  WikitudeSDK.deleteRootCacheDirectory(this); FIXME this should be uncommented

        (new DownloadWtc(this)).execute("tracker.wto");
        (new DownloadWtc(this)).execute("tracker.wtc");

        ((Button) findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start(0);
            }
        });

        ((Button) findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start(1);
            }
        });

        ((Button) findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,ListActivity.class);
                startActivity(i);
            }
        });


    }

    public void start(final int val){

            WikitudeSDK.getPermissionManager().checkPermissions(this, new String[]{Manifest.permission.CAMERA}, PermissionManager.WIKITUDE_PERMISSION_REQUEST, new PermissionManager.PermissionManagerCallback() {
                @Override
                public void permissionsGranted(int requestCode) {
                    Class activity = SimpleImageTrackingActivity.class;
                    if(val==1)
                    activity = ObjectTrackingActivity.class;
                    final Intent intent = new Intent(MainActivity.this, activity);
                    startActivity(intent);
                }

                @Override
                public void permissionsDenied(String[] deniedPermissions) {
                    Toast.makeText(MainActivity.this, "The Wikitude SDK needs the following permissions to enable an AR experience: " + Arrays.toString(deniedPermissions), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void showPermissionRationale(final int requestCode, final String[] permissions) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Wikitude Permissions");
                    alertBuilder.setMessage("The Wikitude SDK needs the following permissions to enable an AR experience: " + Arrays.toString(permissions));
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            WikitudeSDK.getPermissionManager().positiveRationaleResult(requestCode, permissions);
                        }
                    });

                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                }
            });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        WikitudeSDK.getPermissionManager().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}
