package com.wikitude.samples;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.RestrictTo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;
import com.wikitude.nativesdksampleapp.R;

import java.io.IOException;

public class ResultActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;

    @Override
    protected void onPause() {
        if(mediaPlayer.isPlaying())
        mediaPlayer.pause();
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        ((ScrollView) findViewById(R.id.scrollView)).fullScroll(ScrollView.FOCUS_UP); //Fa scroll Up altirmenti parte dal basso
        ((ScrollView) findViewById(R.id.scrollView)).smoothScrollTo(0,0); //Fa scroll Up altirmenti parte dal basso

        final ArtObj artObj = Api.artObj; //FIXME check that artObj is not null
        mediaPlayer = new MediaPlayer();
        (new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mediaPlayer.setDataSource(artObj.getUrl_audio());
                    mediaPlayer.prepareAsync();
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        })).start();

        final VideoView videoView;
        videoView = (VideoView)findViewById(R.id.video);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        (new Thread(new Runnable() {
            @Override
            public void run() {
                videoView.setVideoURI(Uri.parse(artObj.getUrl_video()));
            }
        })).start();


        ((TextView) findViewById(R.id.nome)).setText(artObj.getNome());
        ((TextView) findViewById(R.id.autore)).setText(artObj.getAutore());
        ((TextView) findViewById(R.id.anno_creazione)).setText(artObj.getAnno_creazione());
        ((TextView) findViewById(R.id.descrizione)).setText(artObj.getDescrizione());
        ((TextView) findViewById(R.id.storia)).setText(artObj.getStoria());

        Picasso.get().load(artObj.getUrl_image()).into((ImageView)findViewById(R.id.imageView));

        final Button button_audio = findViewById(R.id.audio);

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                button_audio.setText("RIPRODUCI AUDIO");
            }
        });

        button_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaPlayer.isPlaying()){
                    button_audio.setText("RIPRODUCI AUDIO");
                    mediaPlayer.pause();
                }
                else{
                    button_audio.setText("PAUSA AUDIO");
                    mediaPlayer.start();
                }
            }
        });

    }
}
